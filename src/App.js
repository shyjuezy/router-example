import React from 'react';
import { createStore, combineReducers } from 'redux'
import uuid from 'uuid'
import './App.scss';

// const addExpense = (
//   {
//     description = '',
//     note = '',
//     amount = 0,
//     createdAt = 0
//   } = {}
// ) => ({
//   type: 'ADD_EXPENSE',
//   expense: {
//     id: uuid(),
//     description,
//     note,
//     amount,
//     createdAt
//   }
// });

// const expenseStateDefault = []
// const filterStateDefault = {
//   text: '',
//   sortBy: 'date',
//   startDate: undefined,
//   endDate: undefined
// }

// const expenseReducer = (state = expenseStateDefault, action) => {
//   switch (action.type) {
//     case 'ADD_EXPENSE':
//       return ([...state, action.expense])
//     default:
//       return state
//   }
// }
// const filterReducer = (state = filterStateDefault, action) => {
//   return state
// }

// const store = createStore(
//   combineReducers({
//     expense: expenseReducer,
//     filter: filterReducer
//   })
// )

// store.subscribe(() => console.log(store.getState()))

// const expenseOne = store.dispatch(addExpense({ description: 'Coffee', amount: 350 }))
// store.dispatch(addExpense({ description: 'Cake', amount: 1500 }))

// console.log(expenseOne)

// store.dispatch({
//   type: 'INCREMENT',
//   incrementBy: 10
// })

// const incrementState = ({ incrementBy = 1 } = {}) => {
//   return (
//     {
//       type: 'INCREMENT',
//       incrementBy
//     }
//   )
// }

// const countReducer = ((state = { count: 0 }, action) => {
//     switch (action.type) {
//       case 'INCREMENT':
//         return { count: state.count + action.incrementBy }
//       default:
//         return state
//     }
// })

// const store = createStore(countReducer)

// console.log(store.getState())
// store.subscribe(() => console.log(store.getState()))

// store.dispatch(incrementState())
// store.dispatch(incrementState({ incrementBy: 10 }))

function App() {
  return (
    <div className="App">
      <header>
        <AuthInfo isAuthenticated={false} info='Info Data' />
      </header>
    </div>
  );
}

const withAdminWarning = (WrappedComponent) => {
  return (props) => (
    <div>
      <p>Please do not share this information - {props.info}</p>
      <WrappedComponent {...props} />
    </div>
  )
}

const Info = (props) => {
  return (
    <div>
      <p>The info is : {props.info}</p>
    </div>
  )
}
const requireAuthentication = (WrappedComponent) => {
  return (props) => (
    <div>
      {props.isAuthenticated ? <WrappedComponent {...props}/> : <p>Please Login to view data</p>}
    </div>
  )
}

const AdminInfo = withAdminWarning(Info)
const AuthInfo = requireAuthentication(Info)

export default App;
